---
title: INFO501 -- Système d'exploitation
author: Pierre Hyvernat
patat:
  wrap: true
  margins:
      left: 1
      right: 1
  incrementalLists: true
  slideLevel: 3
  breadcrumbs: true
  theme:
      emph: [italic]
      strong: [dullMagenta, underline]
      code: [dullCyan]
      codeBlock: [dullCyan]
      math: [rgb#aaaaaa, underline]
...


# Gestion des fichiers
### Fichier ?

L'objectif principal des fichiers est :

- de conserver de l'information après la mort d'un processus,
- de conserver des quantités d'information supérieures à la quantité de
  mémoire disponible,
- de partager de l'information.

. . .

On suppose que tous les support de stockage disposent de méthodes 

- `read(k)` pour lire l'octet numéro `k` du support,
- `write(k, o)` pour écrire l'octet `o` à l'emplacement numéro `k`.

. . .

Tous les supports modernes disposent de méthode plus complexes qui permettent
d'améliorer l'efficacité des opérations.

. . .

L'objectif d'un *système de fichiers* est d'offrir une interface simple pour
manipuler des fichiers.

. . .

L'implémentation de cette interface doit pouvoir se
faire sur (presque) n'importe quel support de stockage.



## Interface POSIX

### Répertoires, fichiers, chemins
Les systèmes de fichiers "standards" sont basés sur la notion de *chemin*.

1. Chaque fichier possède un **nom** (et autrefois, une *extension*),
2. chaque fichier se trouve dans un **dossier** (ou **répertoire**) ayant lui
   même un nom,
3. chaque répertoire se trouve dans un autre répertoire, sauf le répertoire
   **racine**,
4. chaque répertoire est accessible depuis la racine.

. . .

Le **chemin d'accès** d'un fichier (ou d'un répertoire) est donné par la suite
des noms de répertoires nécessaires pour y accèder depuis la racine.

. . .

Par exemple `home,hyvernat,Enseignement,INFO507,TD,td2.pdf`

. . .

Sur les système Unix, le séparateur est le `/`, et le chemin précédent est
noté `/home/hyvernat/Enseignement/INFO507/TD/td2.pdf`

. . .

Sous Windows, le séparateur est le `\`, et le chemin est précédé par le nom
de la partition correspondante : `C:\Users\Pikachu\L3\info507\TP1\minishell.c`

. . .

$démo realpath$


### Fonctions POSIX

L'interface POSIX est assez simple, il y a 5 appels système principaux pour
manipuler les fichiers

- `open`        (ou `fopen`)
- `close`       (ou `fclose`)
- `read`        (ou `fread`)
- `write`       (ou `fwrite`)
- `lseek`       (ou `fseek`)

. . .

Le dernier permet de déplacer le "curseur" dans le fichier.

. . .

Les variantes `f...` manipulent des structures haut niveau (`FILE*`) qui sont
bufferisées.

. . .

Les variantes sans `f` manipulent des structures bas niveau (descripteurs
fichiers : type `int`) et ne sont pas bufferisées.

. . .

$démo `write`, `fwrite`, `fseek` et `truncate`$

. . .

Il existe de nombreux autres appels système POSIX, mais ils manipulent plutôt
les **métadonnées** des fichiers : `stat`, `rename`, `unlink`, ...

### `stdin`, `stdout`, `stderr` et redirections

Le système associe à chaque processus 3 descripteur de fichiers (et 3 `FILE*`
correspondant) :

- l'entrée standard : `stdin` (descripteur `0`), en lecture seule
- la sortie standard : `stdout` (descripteur `1`), en écriture seule
- la sortie d'erreur : `stderr` (descripteur `2`), en écriture seule

. . .

Le shell définie plusieurs redirections impliquant ces "fichiers" :

- `COMANDE < FICHIER` : remplace `stdin` par le fichier,
- `COMMANDE > FICHIER` : remplace `stdout` par le fichier,
- `COMMANDE >> FICHIER` : remplace `stdout` par le fichier, ouvert en mode
  "append",
- `COMMANDE 2> FICHIER` et `COMMANDE 2>> FICHIER` : remplace le descripteur
  `2` (`stderr`) par le fichier,
- `COMMANDE1 | COMMANDE2` : remplace `stdin` de `COMMANDE2` par `stdout` de
  `COMMANDE1`,
- ...

. . .

Apprendre à utiliser ces mécanisme vous rendra (nettement) plus efficace dans
un shell !

. . .

$démo dans le shell (avec `maj`, etc.)$

### Fichiers spéciaux (Unix)

Les système Unix ont d'autres *fichiers spéciaux* qui utilisent la même interface.

- les liens symboliques (`ln -s`)
- les tubes (`mkfifo`)
- les sockets

. . .

Les périphériques (dans répertoire `/dev/`) se comportent également comme des
fichiers :

- périphériques d'entrée : ouverts en lecture seule,
- périphériques de sortie : ouverts en écriture seule.

. . .

Il y a également des "faux" périphériques

- `/dev/null` (POSIX)
- `/dev/zero`
- `/dev/random` (bloquant) et `/dev/urandom` (non bloquant)
- `/dev/full`

. . .

$démo$


