### Création d'un processus -- 2


Pour créer un nouveau processus à partir d'un programme, on combine `fork()`
avec l'appel système `execl(exe, arg1, arg2, ...)` qui *remplace* le processus
courant.
(cf `man 3 exec`)



Ça ressemble donc à :

```c
  ...
  int p = fork();
  if (p == 0) {
    execl(..., ..., ...);
  } else if (p > 0) {
    // le processus initial continue
    ...
  } else {
    // gestion d'erreur
    ...
  }
```


### Arborescence des processus

La relation père / fils créée par `fork()` donne une *structure
arborescente* aux processus.



Le processus `init` ou `systemd` a le PID `1` et se
trouve à la racine.



On peut visualiser cette arborescence avec l'utilitaire `pstree`.



`démo pstree / ps --forest`


### Mort d'un processus

Un processus peut se terminer

- normalement (`exit`),
- suite à une erreur,
- ou se faire arrêter par un autre processus (`kill`).



Questions :

1. que deviennent ces fils ?

3. que devient son père ?


### Mort d'un processus

Réponses :

1.  Les fils sont *adoptés* par `init` / `systemd`.



    Souvent, le processus "fait le ménage" et stoppe lui même ses fils.

2.  Le père reçoit un signal qu'un fils c'est terminé, mais il doit l'examiner
    (avec `wait` ou `wait_pid`).



    Tant que son père n'a pas fait de `wait`, un processus arrêté est conservé
    par le système. Il n'utilise plus de ressources et on parle de processus
    *zombie*.



    Le processus `init` / `systemd` fait régulièrement des `wait` pour
    supprimer des processus zombie}



`démo zombie`



### Table des processus

Toutes les informations des processus sont stockées dans la **table des processus**.
Dans le noyau Linux il s'agit d'une liste doublement chainée + tables de hachage.



Plus précisemment, il y a une liste pour les processus bloqués, et des
listes pour les processus prêts.



Cette table permet d'associer à un PID des données comme :

- l'état du processus;
- le pointeur vers la pile du processus;
- les droits du processus;
- le répertoire de travail;
- la priorité;
- la liste des fichiers ouverts;
- un pointeur vers le père, le premier fils, le frère suivant;
- ...


Sous Linux, cette table est accessible par le système de fichiers virtuel
`/proc/`.

<!-- (`man proc` pour avoir tous les détails !) -->



`démo /proc`


# Gestion de la mémoire
### Introduction


Ordres de grandeur des débits d'accès à la mémoire

- cache : quelques centaines à quelques dizaines de GB/s
- RAM : autour de 10 GB/s
- SSD : quelques milliers de MB/s
- clé USB : quelques centaines de MB/s
- disque dur : quelques dizaine de MB/s



Pour avoir un ordinateur rapide, il faut privilégier les mémoires rapides.

- gestion du cache : hardware (processeur)

- gestion de la RAM : software (OS)



### Abstraction

La mémoire "brute" ressemble en général à
```
    0x0000                                            0xffff
     |-  noyau  -|-  proc 1  -|-  proc 2  -|...libre...|
     ***************************************-----------+
```



Le noyau est chargé en premier, et les processus "s'empilent" par
dessus.



Problème : les accès mémoire dépendent de l'emplacement du processus et il est
difficile de modifier la mémoire allouée à un processus.



Pour simplifier, la mémoire est rendue *abstraite*. Chaque processus manipule
des adresses qui commencent à `0x00000000` et il y a une *traduction
d'adresses* pour convertir ces adresses "virtuelles" en adresses physiques.



Cette traduction est faite au niveau hardware par des circuits spécifiques du
processeur (MMU).

<!-- $démo adresse virtuelle$ -->

## Allocation linéaire
### Fonctionnement

Le mode d'adressage par défaut d'un processeur est "linéaire".
C'est le mode d'adressage utilisé pour charger le noyau en mémoire.

La traduction d'adresses est simple : le processeur a 2 registres spéciaux

- `OFFSET` pour l'adresse physique du *début du bloc*
- `LIMIT` pour la taille du bloc


```
    0x0000                                            0xffff
     |***********|-   proc   -|************|...........|
                 ^            |
              OFFSET          |
                 <--- LIMIT -->
```

L'adresse `p` est traduite en l'adresse `OFFSET + p`,
précédée d'un test que `p < LIMIT`.



Normalement, les processus n'ont pas accès à ces registres, mais ce n'était
pas le cas du Intel 8088 !


### Algorithmes d'allocation

La traduction d'adresses est faite par le processeur, mais c'est l'OS qui
gère les registre `OFFSET` et `LIMIT` et qui choisit les blocs.

- politique "First Fit" : premier bloc libre,
- politique "Next Fit" : bloc libre suivant,
- politique "Best Fit" ou "Worst Fit",
- mécanisme des "zones siamoises" ("buddy system" en anglais).



Quel que soit la politique choisie, des trous apparaissent lorsque des
processus meurent. Il devient de plus en plus difficile de d'allouer de la
mémoire.

```
    0x0000                                            0xffff
     |****|..|***|.|**|...|***|.|**|...|***|.....|*****|
```

On parle de *fragmentation externe*.


## Pagination
### Fonctionnement

Pour rendre l'allocation plus souple, le noyau passe le processeur en mode
"mémoire paginée".


La mémoire est divisée en blocs de taille fixe et indivisibles :
les **cadres**.
```
    0x0000                                            0xffff
     |** noyau **|.|.|.|.|.|.|.|.|.|.|.|.|.|.|.|.|.|.|.|
```



Chaque bloc fait 4ko.



Certains processeurs supportent des tailles différentes (4Mo par exemple),
mais ce mode n'existe pas dans le noyau Linux.



La mémoire (virtuelle) d'un processus est découpée en blocs de taille fixe :
les **pages**.



Chaque page *utilisée* est stockée dans un cadre.


### Pagination -- 2

Chaque processus utilisent des adresses arbitraires (entre `0x00000000` et
`0xffffffff` pour un système 32 bits) mais seules une toute petite partie
des ces adresses sont véritablement stockées dans des cadres dans la RAM.



Les cadres ne sont pas forcément consécutifs, ni même dans l'ordre !



L'allocation devient très simple : il suffit de prendre des cadres libres,
n'importe où.



Il n'y a plus de fragmentation externe.



Le noyau Linux utilise le "buddy system" pour ces allocations et peut réclamer
des *plages* de cadres.



Par contre, si un processus ne peut pas réclamer 1 octet unique : il recevra
forcément un nombre entier de cadres.

On peut perdre de la mémoire si les cadres ne sont pas complètement
utilisés.



On parle de *fragmentation interne*.


### Table des pages

Le lien entre pages et cadres se fait dans une structure spécialisée : la
**table des pages**.



C'est un "tableau" qui associe, à chaque page :

- un bit "`present`" pour savoir si la page est présent dans la RAM,
- un numéro de cadre,
- un bit "`accessed`" pour les accès en lecture,
- un bit "`dirty`" pour les accès en écriture,
- un bit "`RW` / `RO`",
- ...



C'est cette table qui permet de faire la traduction d'adresses.



### Table des pages -- 2

Cette table n'est pas stockée comme un tableau, mais comme une **table à
plusieurs niveaux**.

```
  32 bits : ................................
            <--------> 1er niveau (10 bits)
                      <--------> 2eme niveau (10 bits)
                                <----------> offset (12 bits)
```



La table des pages est donc un tableau de tableaux.



Le tableau principal est alloué (taille 1024),



mais toutes ces cases ne sont pas forcément allouées.

### Table des pages -- 3

Sur 64 bits, c'est pareil : on a un tableau de tableaux de tableaux de
tableaux :

```
  64 bits : 0000000000000000................................................
                            <-------> 1er niveau (9 bits)
                                     <-------> 2eme niveau (9 bits)
                                              <-------> 3eme niveau (9 bits)
                                  4eme niveau (9 bits) <------->
                                               offset (12 bits) <---------->
            <--------------> inutilisés (16 bits)
```




### Traduction d'adresses

Pour traduire l'adresse `p` :

1. on prend le *numéro de page* contenant  p : $$n = \frac {p}{2^{12}} = p>>12 $$

2. on lit le numéro de cadre associé à `n` dans la table des pages : `c`

3. on prend l'adresse relative à la page : `o` = `p mod 2^{12}` = `p & 0xfff`

4. on combine `c` et `o :`  $c * 2^{12} + o = c<<12 | o$.



Si l'étape 2 échoue (bit `present` à `0`), la traduction échoue.



On parle de **défaut de page**.



Le processus est bloqué pendant que le système gère le problème
(allocation d'un nouveau cadre, récupération du cadre dans la mémoire
secondaire, ...).
