---
title: "INFO507 -- Système d'exploitation"
author: "Pierre Hyvernat"
patat:
  wrap: true
  margins:
      left: 1
      right: 1
  incrementalLists: true
  slideLevel: 3
  breadcrumbs: true
  theme:
      emph: [italic]
      strong: [dullMagenta, underline]
      code: [dullCyan]
      codeBlock: [dullCyan]
      math: [rgb#aaaaaa, underline]
---



# Avant de commencer

### Organisation du cours

-   CM à distance, sur Discord

-   TD hybrides

-   3 TP obligatoires notés :

    > -   programmation système : écriture d'un shell,
    > -   gestion de la mémoire (utilisateur) : écriture d'un ramasse miettes,
    > -   fichiers : modification d'un système de fichiers à base d'inodes.



    Remarque : vous devez avoir accès à une machine (virtuelle) si vous
    souhaitez faire les TP sur votre portable !

-   Un examen final en semaine 51, en présentiel.

### Pour aller plus loin

-   Tanenbaum : _Systèmes d'exploitation_

-   Bovet et Cesati : _Understanding the Linux Kernel_

-   Arpaci-Dusseau : _Operating Systems: Three Easy Pieces_

# Introduction

### Historique

Les premiers ordinateurs n'avaient pas de système d'exploitation.



L'apparition des systèmes d'exploitation s'est faite progressivement pour
répondre à la complexité croissantes des tâches d'administration.



Les premiers systèmes datent de 1956 : ils faisaient uniquement du
traitement par lots ("batch processing").



IBM a produit plusieurs systèmes à cette époque. (OS/360, DOS/360, TSS/360)



La multiprogrammation et les systèmes multi-utilisateurs apparaissent rapidement
(notamment Multics en 1964, utilisé jusqu'en 2000).



Unix apparait en 1969, et DOS en 1979.



Windows arrive en 1985, et Linux en 1991.

### Qu'est-ce qu'un système d'exploitation

Un système d'exploitation (OS = _"Operating System"_) _virtualise_ les
ressources :

-   les processus
-   la mémoire
-   les fichiers
-   ...



<!-- Une couche plus bas niveau virtualise l'électronique, mais nous n'en parlerons pas. -->



Cette virtualisation a plusieurs rôles :

-   faciliter l'utilisation du matériel par l'utilisateur,
-   offrir des garantie, de sécurité par exemple.



C'est par exemple le système qui gère l'exécution "en parallèle" de plusieurs
programmes.



Nous allons surtout parler des OS "grand public" de la famille Unix, et nous
n'aborderons pas les problèmes "modernes" (multi-coeurs, virtualisation, etc.)



Remarque : la programmation d'un OS nécessite un langage _bas niveau_.

### Le noyau

La partie principale d'un système d'exploitation est le _noyau_.
C'est lui qui gère toute la partie "virtualisation" des ressources.

L'OS contient aussi

-   des bibliothèques de programmation,
-   des utilitaires (shell, gestion des utilisateurs), etc.



Dans les livres on distingue

-   les noyaux monolithiques
-   les micro-noyaux



La frontière entre les deux n'est pas toujours claire. Par exemple, Linux est
un noyau monolithique modulaire.



Vous pouvez obtenir la liste des modules chargés avec la commande `lsmod`.



`démo lsmod`

### Le noyau -- 2 : _"With great power comes great responsibility"_

Les processeurs ont en général au moins 2 modes de fonctionnement,

-   le **mode noyau**, où toutes les opérations sont autorisés,

-   le **mode utilisateur**, où certaines fonctionnalités "dangereuses" sont désactivées.



Certains processeurs (8086 par exemple) ont d'autres modes de
fonctionnement.
Ces modes ne sont pas utilisé par Linux.

<!-- cf understanding the linux kernel p 19 -->



En utilisation normale d'un ordinateur, le processeur est en mode
"utilisateur".



C'est le noyau qui s'occupe de passer _momentanément_ en mode "noyau"
lors de certaines opérations.
(accès aux disques ou autres périphériques, accès à la mémoire, etc.)



Un **appel système** est une opération qui a besoin du mode "noyau" pour
s'effectuer.



`démo strace`

### POSIX

De nombreux OS proposent une interface normalisée : _POSIX_.
(_"Portable Operating System Interface for Unix"_)

-   Unix, Linux, BSD, Minix
-   Android,
-   macOS,



Cette norme définit le shell, les droits utilisateurs ou
divers utilitaires.
(La version 2017 fait 3951 pages.)



Cette norme spécifie un ensemble de fonctions standards (`printf`, `malloc`, `open`, ...)



Elle ne spécifie pas si ces fonctions sont des appels système.



Des appels système typiques sont :

-   les fonctions relatives aux fichiers (création, suppression, modification...)
-   les fonctions relatives à la gestion de la mémoire (`malloc()` et `free()`)
-   les fonctions relatives aux processus (`fork()`)

# Processus

### Processus ?

Un processus, c'est un programme en cours d'exécution.



Chaque processus est identifié par un numéro unique : son **PID**.
(PID = "_Process ID_")



D'autres information sont nécessaires pour l'exécution :

-   priorité,
-   zone mémoire utilisée par le processus,
-   liste des fichiers ouverts, connexions réseaux ou autre,
-   droits du processus, utilisateur à l'origine du processus,
-   etc.



Un processus correspond à un unique exécutable, mais un exécutable peut avoir
plusieurs processus associés à un instant donné.



Certains processus peuvent parfois partager des ressources (mémoire, ...)
entre eux. On parle alors de processus légers, ou threads.

`démo ps / top`

## Ordonnancement

### États d'un processus

À un instant donné, chaque processus est soit :

-   _en exécution_ (dans un système monoprocesseur, il n'y en a au plus un)

-   _bloqué_ (il attend un évènement extérieur pour pouvoir continuer)

-   _prêt_ (suspendu provisoirement pour permettre l'exécution d'un autre processus)



L'état d'un processus évolue :

1.  de en exécution à bloqué lorsqu'une ressource n'est pas disponible
2.  de bloqué à prêt si la ressource devient disponible,
3.  de en exécution à prêt si le système le décide,
4.  de prêt à en exécution si le système le décide.



### États d'un processus : Linux

La réalité est un peu plus complexe : sous Linux, on a les états

-   `TASK_RUNNING` (`R`) : prêt ou bien en exécution,
-   `TASK_INTERRUPTIBLE` (`S`), `TASK_UNINTERRUPTIBLE` (`D`), `TASK_KILLABLE`
    (`D`) ou
    `TASK_TRACED` (`T` ou `t`) : bloqué,,
-   `TASK_STOPPED` (`T`) : terminé,
-   `EXIT_DEAD` : etat transitoire pendant que le noyau fait le ménage,
-   `EXIT_ZOMBIE` (`Z`) : on en reparlera plus tard.

`démo top / ps`

### Ordonnancement

Le système doit choisir régulièrement quel processus (prêt) doit être
exécuté. On parle d'**ordonnancement**.



Les tous premiers systèmes utilisaient un ordonnancement non-préemptif de
type batch : tant que le processus en exécution étaient vivant, il restaient
en exécution.



> `sleep(UINT_MAX);` : oups !



Pour améliorer, lorsque le processus en exécution devient bloqué, on peut le
remplacer par un processus prêt.



> `while(1){}` : oups !



Les systèmes modernes utilisent un ordonnancement **préemptif** : le
système peut stopper un processus en exécution pour le remplacer par un
processus prêt.

<!-- Remarque : le noyau Linux est dit *préemptif* pour une autre raison : il peut
stopper un processus en mode noyau -->



Pour certains ordonnanceurs non-préemptifs, les processus pouvaient utiliser
une fonction `yield` pour laisser le processeur. (Windows 3 par exemple)



On retrouve cette commande pour les processus légers : `sched_yield()`.

### Ordonnancement non-préemptif

L'ordonnancement le plus simple est "FIFO" : les processus arrivent dans une
files et sont pris dans l'ordre.



Un processus qui fait un `yield` ou se retrouve bloqué est passé en fin de
file.



Si on souhaite minimiser le temps d'attente moyen des processus, le meilleur
ordonnancement est le "plus court temps d'exécution en premier".



Problème : comment connaitre le temps d'exécution d'un processus ?



Ces politiques d'ordonnancement peuvent provoquer des **famines** :
certains processus prêts ne s'exécuteront jamais.

### Ordonnancement préemptif

Changer le processus en exécution prend du temps : il faut sauvegarder l'état
des registre et du processus pour pouvoir reprendre l'exécution. On parle de
_context switch_.



La fréquence de ces changements ne doit pas être trop élevée.



L'ordonnancement préemptif le plus simple est le "tourniquet" : les processus arrivent dans une
file et sont pris dans l'ordre.
Ils ne sont exécutés qu'un court lapse de temps avant d'être remis en queue
de file.



Le choix de cet intervalle est empirique (typiquement : quelques dizaines
ou quelques centaines de ms)



> _The choice of quantum duration is always a compromise. The rule of
> thumb adopted by Linux is: choose a duration as long as possible, while
> keeping good system response time._
>      "Understanding the Linux kernel", page 263



Cette politique garantie l'absence de famine !



On peut ajouter une notion de _priorité_ au processus avec
cette politique, mais il y a des subtilités. (cf ordonnanceur de Minix)

### Ordonnanceur de Linux

Linux a utilisé un ordonnanceur "tourniquet" jusqu'à la version 2.6.



Il y avait des quantums de temps variables et une division entre processus
_expirés_ (ils ont fini leur quantum) et processus _actifs_.



Les processus _interactifs_ n'expirent jamais !
(Il faut trouver une heuristique pour reconnaitre ces processus.)

Les processus "temps réel" utilisent soit un "tourniquet" (`SCHED_RR`)
soit une politique "FIFO" (`SCHED_FIFO`).



Depuis la version 2.6.33, Linux utilise un nouvel ordonnanceur : "completely
fair scheduler" (CFS) qui garde trace du temps effectif passé sur le
processeur.

<!-- cf "three easy piece" section cpu-sched-lottery -->



Remarque : l'ordonnanceur utilise des structures de données de type "arbres rouges
et noirs" !



`démo ps / top ???`

## Vie et mort des processus

### Création d'un processus

C'est simple !



Il n'y a qu'une fonction POSIX pour créer un processus : `fork()`.



Cette fonction _duplique_ le processus actuel



-   et renvoie `0` dans le nouveau processus,

-   le PID du processus nouveau processus dans le processus original.



On parle de **processus père** et de **processus fils**.



`démo fork`
