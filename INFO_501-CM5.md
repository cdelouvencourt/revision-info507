---
title: INFO501 -- Système d'exploitation
author: Pierre Hyvernat
patat:
  wrap: true
  margins:
      left: 1
      right: 1
  incrementalLists: true
  slideLevel: 3
  breadcrumbs: true
  theme:
      emph: [italic]
      strong: [dullMagenta, underline]
      code: [dullCyan]
      codeBlock: [dullCyan]
      math: [rgb#aaaaaa, underline]
...

# Gestion des fichiers
### Fichier ?

L'objectif principal des fichiers est :

- de conserver de l'information après la mort d'un processus,
- de conserver des quantités d'information supérieures à la quantité de
  mémoire disponible,
- de partager de l'information.

. . .

On suppose que tous les support de stockage disposent de méthodes 

- `read(k)` pour lire l'octet numéro `k` du support,
- `write(k, o)` pour écrire l'octet `o` à l'emplacement numéro `k`.

. . .

Tous les supports modernes disposent de méthode plus complexes qui permettent
d'améliorer l'efficacité des opérations.

. . .

L'objectif d'un *système de fichiers* est d'offrir une interface simple pour
manipuler des fichiers.

. . .

L'implémentation de cette interface doit pouvoir se
faire sur (presque) n'importe quel support de stockage.



## Interface POSIX

### Répertoires, fichiers, chemins
Les systèmes de fichiers "standards" sont basés sur la notion de *chemin*.

1. Chaque fichier possède un **nom** (et autrefois, une *extension*),
2. chaque fichier se trouve dans un **dossier** (ou **répertoire**) ayant lui
   même un nom,
3. chaque répertoire se trouve dans un autre répertoire, sauf le répertoire
   **racine**,
4. chaque répertoire est accessible depuis la racine.

. . .

Le **chemin d'accès** d'un fichier (ou d'un répertoire) est donné par la suite
des noms de répertoires nécessaires pour y accèder depuis la racine.

. . .

Par exemple `home,hyvernat,Enseignement,INFO507,TD,td2.pdf`

. . .

Sur les système Unix, le séparateur est le `/`, et le chemin précédent est
noté `/home/hyvernat/Enseignement/INFO507/TD/td2.pdf`

. . .

Sous Windows, le séparateur est le `\`, et le chemin est précédé par le nom
de la partition correspondante : `C:\Users\Pikachu\L3\info507\TP1\minishell.c`

. . .

$démo realpath$


### Fonctions POSIX

L'interface POSIX est assez simple, il y a 5 appels système principaux pour
manipuler les fichiers

- `open`        (ou `fopen`)
- `close`       (ou `fclose`)
- `read`        (ou `fread`)
- `write`       (ou `fwrite`)
- `lseek`       (ou `fseek`)

. . .

Le dernier permet de déplacer le "curseur" dans le fichier.

. . .

Les variantes `f...` manipulent des structures haut niveau (`FILE*`) qui sont
bufferisées.

. . .

Les variantes sans `f` manipulent des structures bas niveau (descripteurs
fichiers : type `int`) et ne sont pas bufferisées.

. . .

$démo `write`, `fwrite`, `fseek` et `truncate`$

. . .

Il existe de nombreux autres appels système POSIX, mais ils manipulent plutôt
les **métadonnées** des fichiers : `stat`, `rename`, `unlink`, ...

### `stdin`, `stdout`, `stderr` et redirections

Le système associe à chaque processus 3 descripteur de fichiers (et 3 `FILE*`
correspondant) :

- l'entrée standard : `stdin` (descripteur `0`), en lecture seule
- la sortie standard : `stdout` (descripteur `1`), en écriture seule
- la sortie d'erreur : `stderr` (descripteur `2`), en écriture seule

. . .

Le shell définie plusieurs redirections impliquant ces "fichiers" :

- `COMANDE < FICHIER` : remplace `stdin` par le fichier,
- `COMMANDE > FICHIER` : remplace `stdout` par le fichier,
- `COMMANDE >> FICHIER` : remplace `stdout` par le fichier, ouvert en mode
  "append",
- `COMMANDE 2> FICHIER` et `COMMANDE 2>> FICHIER` : remplace le descripteur
  `2` (`stderr`) par le fichier,
- `COMMANDE1 | COMMANDE2` : remplace `stdin` de `COMMANDE2` par `stdout` de
  `COMMANDE1`,
- ...

. . .

Apprendre à utiliser ces mécanisme vous rendra (nettement) plus efficace dans
un shell !

. . .

$démo dans le shell (avec `maj`, etc.)$

### Fichiers spéciaux (Unix)

Les système Unix ont d'autres *fichiers spéciaux* qui utilisent la même interface.

- les liens symboliques (`ln -s`)
- les tubes (`mkfifo`)
- les sockets

. . .

Les périphériques (dans répertoire `/dev/`) se comportent également comme des
fichiers :

- périphériques d'entrée : ouverts en lecture seule,
- périphériques de sortie : ouverts en écriture seule.

. . .

Il y a également des "faux" périphériques

- `/dev/null` (POSIX)
- `/dev/zero`
- `/dev/random` (bloquant) et `/dev/urandom` (non bloquant)
- `/dev/full`

. . .

$démo$


### Un exemple

Voila un morceau de C qui copie un fichier :
```
    f_from = fopen(from, "r");
    f_to = fopen(to, "w");
    while (1) {
        nread = fread(buffer, 1, buf_size, f_from);
        if (nread == 0) break;
        fwrite(buffer, 1, nread, f_to);
    }
```

. . .

$démo$

. . .

Exercice :

- comparer cette version avec une version utilisant `open`, `read`,
`write` et `close`,
- regardez l'impact du choix de `buf_size` pour les deux versions.


## Système de fichiers

### Allocation continue des données

Le plus "simple" est de stocker les données d'un fichier sur des octets
consécutifs sur le support de stockage :

```
  |*** fichier-1 ***|*** fichier-2 *********|* fichier-3 *| ....
  +-----------------+-----------------------+-------------+-----
```

Ce mécanisme est difficilement utilisable car on ne peut pas facilement
changer la taille d'un fichier !

. . .

Mais il est utilisé !

. . .

Par exemple pour des supports en lecture seule (CD, DVD, bandes).

. . .

Il faudrait bien entendu préciser comment sont stockés les noms de fichiers,
les répertoires, etc.



### Blocs

On découpe les fichiers en **blocs** de taille fixe (typiquement quelques
kio) qui ne sont pas forcément consécutifs.

. . .

La taille des blocs est importante : trop petit et on perd du temps, trop
grand et on perd de l'espace (fragmentation interne)

. . .

Pour retrouver l'ordre des blocs d'un fichier, on utilise

  - une liste chainée de blocs,  
    (Ce n'est pas pratique car l'adresse du bloc suivant utilise une partie du
    bloc.)

  - une FAT ("File Allocation Table") stockée en début de partition,  
    (Un tableau qui donne, pour chaque numéro de bloc, le numéro de
    bloc suivant.)

  - des **inodes** stockés en début de partition.  
    (Une structure de données *de taille fixe* qui représente un
    fichier.)

<!-- 
. . .


$démo dumpe2fs ???$

-->



### Les inodes

Un inode est une *structure* avec plusieurs champs : la taille du fichier, les
droits, le propriétaire, etc.

. . .

(Attention, le *nom* du fichier ne fait pas partie de l'inode !)

Les champs les plus importants sont

- une dizaine de pointeurs vers les premiers blocs de données (*accès direct*)
- un pointeur vers un bloc d'adresses (*une indirection*)
- un pointeur vers un bloc d'adresses de niveau 2 (*deux indirections*)
- un pointeur vers un bloc d'adresses de niveau 3 (*trois indirections*)

L'accès aux blocs du début du fichier se fait directement, et l'accès aux
blocs suivant prend juste un peu plus de temps.


## Exemples
### EXT2

La taille des inodes est fixe. Chaque inode est identifé par son numéro (`ls
-i`) et contient :

 - des métainformations
     - type de fichier (répertoire, lien symbolique, etc.)
     - taille des données
     - propriétaire, droits
     - date de création / modification / accès
 - un *compteur de liens* (`0` pour les inodes non utilisés)
 - un tableau de 15 adresses de blocs :
    - 12 adresses de blocs de données (accès direct)
    - 1 adresse de bloc d'adresses de blocs de données (une indirection)
    - 1 adresse de bloc d'adresses de blocs d'adresses de blocs de données (deux indirections)
    - 1 adresse de bloc d'adresses ... de blocs de données (trois indirections)

. . .

La racine a toujours le numéro d'inode 2.

. . .

$démo, ls -i,  fichier creux (truncate)$

### EXT2 : répertoires

Les répertoires ont aussi des inodes, de type "répertoire".

. . .

Les données d'un répertoire contiennent une liste d'entrées associant un nom
et un numéro d'inode.

. . .

Chaque entrée contient

- un numéro d'inode
- la taille du nom de fichier correspondant
- le nom de fichier correspondant

Il y a toujours 2 entrées pour `.` et `..` !

. . .

Il est possible de réutiliser un numéro d'inode *de fichier standard* dans
plusieurs répertoires. On parle de *lien physique* (*hard link*). (`ln`)

. . .

Les *liens symboliques* utilisent leur données pour stocker un chemin d'accès
vers un autre fichier. (`ln -s`)

$démo lien physique / lien symbolique$

### EXT3, EXT4

Le système EXT3 a remplacé EXT2 presque partout au début des années 2000.

. . .

La différence principale est l'introduction d'un *journal*.

. . .

Le journal loggue les opérations sur le système de fichiers pour limiter les
problème en cas d'arrêt inopiné.

. . .

EXT4 remplace EXT3.

. . .

En plus du journal, EXT4 n'adresse pas simplement des blocs de données, mais
des *plages* de blocs (*extends* end anglais).

. . .

Le mécanisme de stockage des extends est assez différent de celui de EXT2 /
EXT3.

. . .

Le système EXT4 utilise aussi des sommes de contrôle pour améliorer la
fiabilité du système (sur les entrées du journal et les métadonnées).

<!-- vim600:set fileencoding=UTF-8 spelllang=fr spell foldlevel=2: -->
### FAT32

Les données ne sont pas consécutive et on utilise une FAT.


Chaque entrée de fichier est codée sur 32 octets :

- 8 octets pour le nom et 3 octets pour l'extension (complétés par des
  espaces si besoin)
- 1 octet pour les attributs (RO, caché, fichier système, répertoire, archivage?)
- ... (10 octets inutilisés)
- 2 octets pour la date, et 2 octets pour l'heure
- 2 octets pour le numéro du premier bloc
- 4 octets pour la taille du fichier

. . .

Il y a eu plusieurs version du système de fichiers :

- FAT-12 : les numéros de blocs font 12 bits (2 numéros étaient codés sur 3
  octets !)
- FAT-16 : les numéros de blocs font 16 bits
- FAT-32 : les numéros de blocs font ... 28 bits !

. . .

D'autre part, la taille des blocs est un multiple de 512 octets.


### CDROM (ISO9660)

<!--
Les secteurs de données font 2048 octets, mais sont contenus dans des *secteurs logiques* de 2352 octets.

. . .

  - 16 secteurs initiaux : non définis par la norme (zone de boot, ou autre chose)

  - 1 secteur "descripteur primaire" :

    - plusieurs champs d'identification
    - taille des blocs (2048 en général) et nombre de blocs utilisés
    - numéro du bloc de la racine du système
-->

Les répertoires commencent par une liste d'entrées contenant une
dizaine de champs :

- 1 octet pour la longueur de l'entrée
- ... (1 octet : longueur des attributs étendus)
- 8 octets pour l'emplacement du fichier
- 8 octets pour la taille du fichier
- 7 octets pour la date et l'heure d'enregistrement
- 1 octets de drapeaux divers, dont le bit répertoire / fichier
- ... (2 octets : entrelacement)
- ... (4 octets : numéro de disque)
- 1 octet pour la longueur du nom de fichier
- quelques octets pour le nom du fichier : `NOM.EXT;VERSION`
  (n'utilisant que `ABCDEFGHIKLMNOPQRSTUVWXYZ_0123456789`)
- ... (octet facultatif de rembourrage)
- ... (octets facultatifs système)

<!-- Les entrées sont données dans l'ordre alphabétique, sauf pour `.` et `..`
qui viennent en premier. -->

L'adresse de l'entrée de la racine est contenu dans les premiers blocs du
support.

. . .

Remarque : les champs binaires sont codés 2 fois : en "little-endian" et "big-endian" !


